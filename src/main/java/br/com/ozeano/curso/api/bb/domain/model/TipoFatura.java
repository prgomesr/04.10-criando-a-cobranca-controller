package br.com.ozeano.curso.api.bb.domain.model;

public enum TipoFatura {

	RECEITA, DESPESA;
	
}
