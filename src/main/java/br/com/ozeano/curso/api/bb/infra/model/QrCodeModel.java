package br.com.ozeano.curso.api.bb.infra.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QrCodeModel {

	private String url;
	private String txId;
	private String emv;
	
}
