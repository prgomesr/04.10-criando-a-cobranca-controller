package br.com.ozeano.curso.api.bb.domain.model;

import javax.persistence.Embedded;
import javax.persistence.Entity;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Pessoa extends BaseEntity {

	private String nome;
	private String documento;
	private boolean pessoaFisica;
	
	@Embedded
	private Endereco endereco;
	
}
